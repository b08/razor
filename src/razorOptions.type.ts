export type LineFeed = "\n" | "\r\n" | "\r";
export type Language = "ts" | "js";
export type Quotes = "\"" | "'";

export interface RazorOptions {
  language?: Language;
  linefeed?: LineFeed;
  quotes?: Quotes;
}
