import { parse } from "./parse/parse";
import { produce } from "./produce/produce";
import { RazorOptions } from "./razorOptions.type";
import { ContentFile } from "./types";

function defaultOptions(options: RazorOptions): RazorOptions {
  return {
    linefeed: "\n",
    quotes: "\"",
    language: "ts",
    ...options
  };
}

export function generateRazor(template: string, options: RazorOptions = {}): string {
  options = defaultOptions(options);
  const root = parse(template);
  return produce(root, options);
}

export function generate<T extends ContentFile>(file: T, options: RazorOptions = {}): T {
  options = defaultOptions(options);
  return {
    ...file,
    name: file.name + ".rzr",
    contents: generateRazor(file.contents, options),
    extension: "." + options.language
  };
}
