export enum NodeType {
  Literal = "Literal", // content
  Quote = "Quote", // basic
  Apostrophe = "Apostrophe", // basic
  Expression = "Expression", // content
  Injection = "Injection", // content
  Eol = "Eol", // basic
  ForceEol = "ForceEol", // basic
  For = "For",
  If = "If",
  Comment = "Comment", // basic
  Partial = "Partial"
}

export class BasicNode {
  constructor(public type: NodeType, public position: number) { }
}

export class ContentNode extends BasicNode {
  constructor(type: NodeType, public content: string, position: number) {
    super(type, position);
  }
}

export class ForNode extends BasicNode {
  constructor(public condition: string, public children: BasicNode[], position: number) {
    super(NodeType.For, position);
  }
}

export class IfNode extends BasicNode {
  constructor(public condition: string, public ifChildren: BasicNode[], public elseChildren: BasicNode[], position: number) {
    super(NodeType.If, position);
  }
}

export class PartialNode extends BasicNode {
  constructor(public name: string, public parameters: string, public indent: string, position: number) {
    super(NodeType.Partial, position);
  }
}
