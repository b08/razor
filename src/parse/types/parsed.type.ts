import { BasicNode } from "../nodes/nodes.type";
import { ParsedConfig } from "./parsedConfig.type";

export interface Parsed {
  config: ParsedConfig;
  content: BasicNode[];
}

