import { BasicNode } from "../nodes/nodes.type";
import { Token } from "./token.type";

export interface ParseModel {
  nodes: BasicNode[];
  tokens: Token[];
  position: number;
  isEndReached: boolean;
}
