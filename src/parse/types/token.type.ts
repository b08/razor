import { Token } from "@b08/tokenize";
export { Token } from "@b08/tokenize";

export function token(token: string, position: number): Token {
  return { token, position };
}
