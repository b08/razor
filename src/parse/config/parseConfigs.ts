import { ParsedConfig, Token } from "../types";
import { getConfigLine } from "./getConfigLine";
import { ConfigResult } from "./configResult.type";
import { skipBlankLines } from "./skipBlankLines";
import { updateConfig } from "./updateConfig";

function defaultConfig(): ParsedConfig {
  return {
    imports: [],
    definition: {
      position: -1,
      name: "generator",
      parameters: ""
    }
  };
}

export function parseConfig(tokens: Token[]): ConfigResult {
  let index = 0;
  let config = defaultConfig();

  while (index < tokens.length) {
    index = skipBlankLines(tokens, index);
    const line = getConfigLine(tokens, index);
    if (line == null) { break; }
    config = updateConfig(config, line);
    index += line.lineLength;
  }
  return {
    contentIndex: index,
    config
  };
}
