import { extractContent } from "../common/extractContent";
import { findNonSpaceTokenIndex } from "../common/isSpace";
import { Token } from "../types";
import { ConfigLine } from "./configLine.type";
import { isConfigToken } from "./updateConfig";
import { findEolIndex } from "../common/isEol";

export function getConfigLine(tokens: Token[], startIndex: number): ConfigLine {
  const index = findNonSpaceTokenIndex(tokens, startIndex);
  const startingToken = tokens[index];
  if (!isConfigToken(tokens[index])) { return null; }
  const end = findEolIndex(tokens, index);
  const config = extractContent(tokens, index + 1, end);
  return {
    startingToken,
    config,
    lineLength: end - startIndex + 1
  };
}
