import { BasicNode } from "../nodes/nodes.type";

export interface BodyResult {
  endPosition: number;
  nodes: BasicNode[];
  ateLinefeed: boolean;
}
