import { contentTokens, EscapeBrace, Else } from "../tokens";
import { Token } from "../types";
import { except } from "@b08/array";

const flowTokens = except(contentTokens, [EscapeBrace, Else]);

const roots = new Set(flowTokens);

const childTokens = [...flowTokens, "}"];

const children = new Set(childTokens);

export type isFlowToken = (token: Token) => boolean;

export function isRootFlowToken(token: Token): boolean {
  return roots.has(token.token);
}

export function isChildFlowToken(token: Token): boolean {
  return children.has(token.token);
}
