import { isEol } from "../../common/isEol";
import { findNonSpaceTokenIndex } from "../../common/isSpace";
import { BodyState } from "../bodyState.type";
import { treatTokenAsLiteral } from "./content/addLiteral";
import { isFirstCharacterOnTheLine, removeIndent } from "./getIndent";

export function finishChildren(state: BodyState): BodyState {
  return state.isMultiLine
    ? finishMultilineChildren(state)
    : finishSingleLineChildren(state);
}

function finishMultilineChildren(state: BodyState): BodyState {
  if (!isFirstCharacterOnTheLine(state)) {
    return treatTokenAsLiteral(state);
  }
  const after = state.currentIndex + 1;
  const nonSpace = findNonSpaceTokenIndex(state.tokens, after);
  state = removeIndent(state);
  if (nonSpace >= state.tokens.length || isEol(state.tokens[nonSpace])) {
    return {
      ...state,
      eatsEol: true,
      endReached: true,
      currentIndex: nonSpace + 1
    };
  }
  return finishSingleLineChildren(state);
}

function finishSingleLineChildren(state: BodyState): BodyState {
  return {
    ...state,
    eatsEol: false,
    endReached: true,
    currentIndex: state.currentIndex + 1
  };
}
