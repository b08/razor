import { extractContent } from "../../../common/extractContent";
import { findTokenIndex } from "../../../common/findIndex";
import { Token } from "../../../types";
import { findClosingTokenIndex } from "../closing/findClosingTokenIndex";

export interface ConditionResult {
  condition: string;
  success: boolean;
  nextIndex: number;
}

export function getCondition(tokens: Token[], index: number): ConditionResult {
  const parenthesis = findTokenIndex(tokens, index, "(");
  const parenthesisEnd = findClosingTokenIndex(tokens, parenthesis);
  if (parenthesis >= tokens.length || parenthesisEnd >= tokens.length) {
    return { success: false, nextIndex: tokens.length, condition: null };
  }

  return {
    condition: extractContent(tokens, parenthesis + 1, parenthesisEnd).trim(),
    nextIndex: parenthesisEnd + 1,
    success: true
  };
}
