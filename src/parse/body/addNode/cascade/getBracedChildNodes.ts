import { Token } from "../../../types";
import { findTokenIndex } from "../../../common/findIndex";
import { findNonSpaceTokenIndex } from "../../../common/isSpace";
import { isEol } from "../../../common/isEol";
import { BasicNode } from "../../../nodes/nodes.type";
import { parseChildren } from "../../parseBody";
import { containsEol } from "./containsEol";

export interface ChildNodesResult {
  nodes: BasicNode[];
  success: boolean;
  nextIndex: number;
  isMultiline: boolean;
}

export function getBracedChildNodes(tokens: Token[], index: number): ChildNodesResult {
  const brace = findTokenIndex(tokens, index, "{");
  if (brace >= tokens.length) {
    return { nextIndex: brace, nodes: null, success: false, isMultiline: false };
  }
  const nonSpace = findNonSpaceTokenIndex(tokens, brace + 1);
  const startsWithEol = isEol(tokens[nonSpace]);
  const childrenStart = (startsWithEol ? nonSpace : brace) + 1;
  const childrenResult = parseChildren(tokens, childrenStart, startsWithEol);
  const nextIndex = getNextNodeStart(tokens, childrenResult.endPosition, startsWithEol);
  return {
    nodes: childrenResult.nodes,
    nextIndex,
    success: true,
    isMultiline: startsWithEol || containsEol(childrenResult.nodes)
  };
}

function getNextNodeStart(tokens: Token[], end: number, startsWithEol: boolean): number {
  if (!startsWithEol) { return end; }
  const nonSpace = findNonSpaceTokenIndex(tokens, end);
  if (nonSpace < tokens.length && isEol(tokens[nonSpace])) {
    return nonSpace + 1;
  }

  return end;
}
