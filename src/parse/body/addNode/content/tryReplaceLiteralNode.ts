import { BodyState } from "../../bodyState.type";
import { ContentNode, BasicNode, NodeType } from "../../../nodes/nodes.type";
import { last } from "@b08/array";
import { addNode } from "../addNode";

export function tryReplaceLiteralNode(state: BodyState, end: number, replacer: (literal: string) => BasicNode): BodyState {
  const lastNode = last(state.nodes);
  if (lastNode == null || lastNode.type !== NodeType.Literal) {
    return addNode(state, end, replacer(""));
  }

  const literal = (lastNode as ContentNode).content;
  return addNode(trimNode(state), end, replacer(literal));
}

export function trimNode(state: BodyState): BodyState {
  return {
    ...state,
    nodes: state.nodes.slice(0, state.nodes.length - 1)
  };
}
