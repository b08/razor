import { contains, except } from "@b08/array";
import { contentTokens, restOfTokens } from "../../../../tokens";
import { BodyState } from "../../../bodyState.type";
import { findClosingTokenIndex } from "../../closing/findClosingTokenIndex";

const openingTokens = ["<", "(", "["];
const closingTokens = except([...contentTokens, ...restOfTokens], openingTokens);
const closings = new Set(closingTokens);

export function findImplicitExpressionEnd(state: BodyState): number {
  let current = state.currentIndex + 1;
  while (current < state.tokens.length) {
    const token = state.tokens[current];
    if (closings.has(token.token)) {
      break;
    }

    if (contains(openingTokens, token.token)) {
      current = findClosingTokenIndex(state.tokens, current);
    }

    current++;
  }

  return current;
}
