import { extractContent } from "../../../../common/extractContent";
import { ContentNode, NodeType } from "../../../../nodes/nodes.type";
import { BodyState } from "../../../bodyState.type";
import { currentPosition } from "../../../tokensState/currentToken";
import { addNode } from "../../addNode";
import { findClosingTokenIndex } from "../../closing/findClosingTokenIndex";

export function addExplicitExpressionNode(state: BodyState): BodyState {
  let end = findClosingTokenIndex(state.tokens, state.currentIndex);
  const content = extractContent(state.tokens, state.currentIndex + 1, end);
  const node = new ContentNode(NodeType.Expression, content, currentPosition(state));
  return addNode(state, end + 1, node);
}
