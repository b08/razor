import { BasicNode } from "../../nodes/nodes.type";
import { BodyState } from "../bodyState.type";

export function addNode(state: BodyState, newIndex: number, node: BasicNode): BodyState {
  return {
    ...state,
    nodes: [...state.nodes, node],
    currentIndex: newIndex,
    endReached: newIndex >= state.tokens.length
  };
}
