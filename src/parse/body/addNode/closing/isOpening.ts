import { contains, last } from "@b08/array";
import { get, IMap } from "@b08/object-map";
import { Apostrophe, AtBrace, AtParenthesis, Quote } from "../../../tokens";
import { ClosingState } from "./closingState.type";

const allOpenings = ["<", "(", "[", "{", Quote, Apostrophe];

export const possibleOpenings: IMap<string[]> = {
  "<": ["<"],
  "(": allOpenings,
  "{": allOpenings,
  "[": allOpenings,
  [AtParenthesis]: allOpenings,
  [AtBrace]: allOpenings,
  [Apostrophe]: [],
  [Quote]: []
};

export function isOpening(state: ClosingState, token: string): boolean {
  const current = last(state.bracketsStack);
  const openings = get(possibleOpenings, current);
  return contains(openings, token);
}
