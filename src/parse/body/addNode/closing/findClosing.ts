import { ClosingState } from "./closingState.type";
import { isOpening } from "./isOpening";
import { getClosures } from "./getClosures";

export function findClosing(state: ClosingState): ClosingState {
  const closures = getClosures(state.bracketsStack);
  let current = state.currentIndex + 1;
  while (current < state.tokens.length) {
    const token = state.tokens[current].token;
    if (closures.has(token)) {
      return closeBracket(state, current, closures.get(token));
    }

    if (isOpening(state, token)) {
      const subState = { ...state, currentIndex: current, bracketsStack: [...state.bracketsStack, token] };
      const response = findClosing(subState);
      if (response.bracketsStack.length < state.bracketsStack.length) {
        return response;
      }
      current = response.currentIndex;
    }

    current++;
  }

  return {
    ...state,
    bracketsStack: state.bracketsStack.slice(0, state.bracketsStack.length - 1),
    currentIndex: current
  };
}

function closeBracket(state: ClosingState, tokenIndex: number, closedBracketIndex: number): ClosingState {
  return {
    ...state,
    bracketsStack: state.bracketsStack.slice(0, closedBracketIndex),
    currentIndex: tokenIndex
  };
}
