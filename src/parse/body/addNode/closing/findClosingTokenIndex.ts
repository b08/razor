import { Token } from "../../../types";
import { ClosingState } from "./closingState.type";
import { findClosing } from "./findClosing";
import { closingPairs } from "./getClosures";

export function findClosingTokenIndex(tokens: Token[], currentIndex: number): number {
  if (currentIndex >= tokens.length) { return currentIndex; }
  const token = tokens[currentIndex];
  if (!closingPairs.has(token.token)) { throw new Error("Should never happen"); }
  const state: ClosingState = {
    tokens,
    currentIndex: currentIndex,
    bracketsStack: [token.token]
  };
  const result = findClosing(state);
  return result.currentIndex;
}


