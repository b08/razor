import { BasicNode, NodeType } from "../../nodes/nodes.type";

export function isEolNode(node: BasicNode): boolean {
  return node.type === NodeType.Eol;
}
