import { extractContent } from "../../../common/extractContent";
import { findTokenIndex } from "../../../common/findIndex";
import { PartialNode } from "../../../nodes/nodes.type";
import { BodyState } from "../../bodyState.type";
import { currentPosition } from "../../tokensState/currentToken";
import { addNode } from "../addNode";
import { findClosingTokenIndex } from "../closing/findClosingTokenIndex";
import { treatTokenAsLiteral } from "../content/addLiteral";
import { getIndent, removeIndent } from "../getIndent";
import { getIndexAfterSpecialNode } from "./getIndexAfterSpecialNode";

export function addPartialNode(state: BodyState): BodyState {
  let parenthesis = findTokenIndex(state.tokens, state.currentIndex, "(");
  const name = extractContent(state.tokens, state.currentIndex + 1, parenthesis).trim();
  if (parenthesis >= state.tokens.length || name.length === 0) { return treatTokenAsLiteral(state); }

  const parenthesisEnd = findClosingTokenIndex(state.tokens, parenthesis);

  const params = extractContent(state.tokens, parenthesis + 1, parenthesisEnd).trim();
  const indent = getIndent(state);
  state = removeIndent(state);

  let closesAt = findTokenIndex(state.tokens, parenthesisEnd, "]");
  if (closesAt >= state.tokens.length) { closesAt = parenthesisEnd; }
  const nextIndex = getIndexAfterSpecialNode(state, closesAt + 1);
  return addNode(state, nextIndex, new PartialNode(name, params, indent, currentPosition(state)));
}
