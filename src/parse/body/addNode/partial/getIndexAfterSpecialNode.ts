import { BodyState } from "../../bodyState.type";
import { findNonSpaceTokenIndex, findPrevNonSpaceTokenIndex } from "../../../common/isSpace";
import { isEol } from "../../../common/isEol";

export function getIndexAfterSpecialNode(state: BodyState, closesAt: number): number {
  if (!isOnlyNodeOnTheLine(state, closesAt)) {
    return closesAt;
  }
  return findNonSpaceTokenIndex(state.tokens, closesAt) + 1;
}

function isOnlyNodeOnTheLine(state: BodyState, closesAt: number): boolean {
  return onlySpacesBeforeStart(state) && onlySpacesAfterClose(state, closesAt);
}

function onlySpacesBeforeStart(state: BodyState): boolean {
  const nonSpace = findPrevNonSpaceTokenIndex(state.tokens, state.currentIndex - 1);
  return nonSpace < 0 || isEol(state.tokens[nonSpace]);
}

function onlySpacesAfterClose(state: BodyState, closesAt: number): boolean {
  const nonSpace = findNonSpaceTokenIndex(state.tokens, closesAt);
  return nonSpace >= state.tokens.length || isEol(state.tokens[nonSpace]);
}
