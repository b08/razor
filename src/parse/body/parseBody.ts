import { Token } from "../types";
import { addFlowNode } from "./addNode/addFlowNode";
import { BodyResult } from "./bodyResult.type";
import { BodyState } from "./bodyState.type";
import { isFlowToken, isRootFlowToken, isChildFlowToken } from "./isFlowToken";
import { addLiteral } from "./addNode/content/addLiteral";

export function parseBody(tokens: Token[], index: number): BodyResult {
  return parseBodyImpl(tokens, index, isRootFlowToken, false);
}

export function parseChildren(tokens: Token[], index: number, isMultiLine: boolean): BodyResult {
  return parseBodyImpl(tokens, index, isChildFlowToken, isMultiLine);
}

function parseBodyImpl(tokens: Token[], index: number, isFlowToken: isFlowToken, isMultiLine: boolean): BodyResult {
  let state: BodyState = { tokens, currentIndex: index, nodes: [], endReached: index >= tokens.length, isMultiLine, eatsEol: false };

  while (!state.endReached) {
    state = addLiteral(state, isFlowToken);
    if (state.endReached) { return getResult(state); }
    state = addFlowNode(state);
  }

  return getResult(state);
}

function getResult(state: BodyState): BodyResult {
  return {
    endPosition: state.currentIndex,
    nodes: state.nodes,
    ateLinefeed: state.eatsEol
  };
}
