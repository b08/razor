import { Token } from "../../types";

export interface TokensState {
  tokens: Token[];
  currentIndex: number;
}
