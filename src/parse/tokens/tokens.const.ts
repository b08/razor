import * as keywords from "./keywords.const";

export const configTokens = [keywords.Using, keywords.Import, keywords.Define];

export const contentTokens = [
  keywords.At,
  keywords.Quote, keywords.Apostrophe,
  keywords.LineFeed1, keywords.LineFeed2, keywords.LineFeed3,
  keywords.AtParenthesis,
  keywords.AtBrace,
  keywords.AtBracket,
  keywords.EscapeBrace,
  keywords.AtStar,
  // keywords.Eol, keywords.Eof,
  keywords.If, keywords.Else, keywords.For, keywords.Foreach
];

export const restOfTokens = [
  keywords.AtAt, keywords.AtApostrophe, keywords.AtQuote,
  "<", "(", "[", "{", ">", ")", "]", "}",
  " ", "=", "\\\\", "\\\"", "\\'", "*", ",", ";", ":"
];

export const allTokens = [
  ...configTokens,
  ...contentTokens,
  ...restOfTokens
];

