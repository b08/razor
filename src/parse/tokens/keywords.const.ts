// tslint:disable:variable-name
export const Quote = '"';
export const Apostrophe = "'";

export const At = "@";
export const AtQuote = At + Quote;
export const AtApostrophe = At + Apostrophe;
export const AtAt = At + At;
export const AtParenthesis = At + "(";
export const AtBrace = At + "{";
export const EscapeBrace = At + "}";
export const AtBracket = At + "[";
export const AtStar = At + "*";
export const LineFeed1 = "\n";
export const LineFeed2 = "\r\n";
export const LineFeed3 = "\r";
// export const Eol = At + "eol";
// export const Eof = At + "eof";


export const If = At + "if";
export const Else = "else";
export const For = At + "for";
export const Foreach = At + "foreach";

// definition
export const Define = At + "define ";

// js/ts specific
export const Import = At + "import ";  // equivalents
export const Using = At + "using ";

// c# specific
// const Namespace = At + "namespace "
