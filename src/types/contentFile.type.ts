
export interface ContentFile {
  contents: string;
  name: string;
  extension: string;
  folder: string;
}
