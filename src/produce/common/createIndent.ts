import { indentSize } from "./indentSize.const";

const indents = new Map<number, string>();

export function createIndent(amount: number = 1): string {
  if (indents[amount]) { return indents[amount]; }
  return indents[amount] = new Array(amount * indentSize).fill(" ").join("");
}
