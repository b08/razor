import { StringGen } from "./stringGen";
import { disclaimer } from "../../disclaimer/disclaimer";

export function startFile(lineFeed: string): StringGen {
  let gen = new StringGen(lineFeed);
  return gen.append(disclaimer(lineFeed));
}
