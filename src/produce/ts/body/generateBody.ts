import { Parsed } from "../../../parse/types";
import { StringGen } from "../../common/stringGen";
import { generateNode } from "./generateNode";

export function generateBody(gen: StringGen, parsed: Parsed): StringGen {
  gen = parsed.content.reduce(generateNode, gen);
  gen = gen.appendLine(`return gen;`);
  return gen;
}
