import { StringGen } from "../../common/stringGen";
import { ContentNode, PartialNode, ForNode, BasicNode, IfNode } from "../../../parse/nodes/nodes.type";
import { prefixedParams } from "../paramsLine";
import { generateNode } from "./generateNode";

export function literal(gen: StringGen, node: ContentNode): StringGen {
  const content = node.content.replace(/`/g, "\\\`");
  return gen.appendLine(`gen = gen.append(\`${content}\`);`);
}

export function eol(gen: StringGen): StringGen {
  return gen.appendLine("gen = gen.eol();");
}

export function quote(gen: StringGen): StringGen {
  return gen.appendLine(`gen = gen.quote();`);
}

export function apostrophe(gen: StringGen): StringGen {
  return gen.appendLine(`gen = gen.apostrophe();`);
}

export function expression(gen: StringGen, node: ContentNode): StringGen {
  return gen.appendLine(`gen = gen.append((${node.content}).toString());`);
}

export function injection(gen: StringGen, node: ContentNode): StringGen {
  return gen.appendLine(node.content);
}

export function partial(gen: StringGen, node: PartialNode): StringGen {
  const callName = node.name !== "self"
    ? node.name + "."
    : "";
  return gen
    .appendLine(`gen = gen.withIndent(\`${node.indent}\`, g => ${callName}generateContent(${prefixedParams(node.parameters)}g));`);
}

export function forNode(gen: StringGen, node: ForNode): StringGen {
  gen = gen.append(`for (${node.condition}) `);
  return gen.braces(gen => nodeChildren(gen, node.children));
}

export function ifNode(gen: StringGen, node: IfNode): StringGen {
  gen = gen.appendLine(`if (${node.condition}) {`);
  gen = gen.pushIndent();
  gen = nodeChildren(gen, node.ifChildren);
  gen = gen.popIndent();
  if (node.elseChildren && node.elseChildren.length) {
    gen = gen.appendLine("} else {");
    gen = gen.pushIndent();
    gen = nodeChildren(gen, node.elseChildren);
    gen = gen.popIndent();
  }
  return gen.appendLine("}");
}

function nodeChildren(gen: StringGen, nodes: BasicNode[]): StringGen {
  return nodes.reduce((gen, child) => generateNode(gen, child), gen);
}
