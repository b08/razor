import { Parsed } from "../../parse/types";
import { RazorOptions } from "../../razorOptions.type";
import { startFile } from "../common/startFile";
import { generateImports } from "./generateImports";
import { generateContentFunction } from "./generateContentFunction";
import { generateEntryFunction } from "./generateEntryFunction";
import { generateExport } from "./generateExport";

export function produceTs(parsed: Parsed, options: RazorOptions): string {
  let gen = startFile(options.linefeed);
  gen = generateImports(gen, parsed, options);
  gen = gen.appendLine();
  gen = generateContentFunction(gen, parsed);
  gen = gen.appendLine();
  gen = generateEntryFunction(gen, parsed);
  gen = gen.appendLine();
  gen = generateExport(gen, parsed);
  return gen.toString();
}
