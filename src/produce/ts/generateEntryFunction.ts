import { Parsed } from "../../parse/types";
import { StringGen } from "../common/stringGen";
import { paramsLine } from "./paramsLine";

export function generateEntryFunction(gen: StringGen, parsed: Parsed): StringGen {
  gen = gen.append(`function generate(${paramsLine(parsed)}options: GeneratorOptions): string `);
  return gen.braces(gen => gen.appendLine(`return generateContent(${getParametersLine(parsed)} new Generator(options)).toString();`));
}

function getParametersLine(parsed: Parsed): string {
  const line = parsed.config.definition.parameters.split(", ");
  const result = line.map(stripType).join(", ");
  return result.length > 0 ? result + "," : "";
}

function stripType(parm: string): string {
  const semicolon = parm.indexOf(":");
  if (semicolon === -1) { return parm; }
  return parm.slice(0, semicolon).trim();
}
