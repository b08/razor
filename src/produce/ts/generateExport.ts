import { Parsed } from "../../parse/types";
import { StringGen } from "../common/stringGen";

export function generateExport(gen: StringGen, parsed: Parsed): StringGen {
  gen = gen.append(`export const ${parsed.config.definition.name} = `);
  return gen.bracesSemicolon(gen => generateExportContent(gen));
}

function generateExportContent(gen: StringGen): StringGen {
  gen = gen.appendLine("generate,");
  return gen.appendLine("generateContent");
}

