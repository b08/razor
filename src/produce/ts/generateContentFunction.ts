import { Parsed } from "../../parse/types";
import { StringGen } from "../common/stringGen";
import { paramsLine } from "./paramsLine";
import { generateBody } from "./body/generateBody";

export function generateContentFunction(gen: StringGen, parsed: Parsed): StringGen {
  gen = gen.append(`function generateContent(${paramsLine(parsed)}gen: Generator): Generator `);
  return gen.braces(gen => generateBody(gen, parsed));
}
