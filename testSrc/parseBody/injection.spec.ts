import * as expectNode from "./expectNode";
import { parse } from "../../src/parse/parse";
import { describe } from "@b08/test-runner";

describe("parser/injection", it => {

    it("should return injection node", async expect => {
        // arrange
        const src = "aaa@{let a = b} ccc";

        // act
        const res = parse(src);

        // assert
        expect.equal(res.content.length, 3);
        expectNode.literal(expect, res.content[0], "aaa");
        expectNode.injection(expect, res.content[1], "let a = b");
        expectNode.literal(expect, res.content[2], " ccc");
    });


    it("should return deep brackets in injection", async expect => {
        // arrange
        const src = `@{let x = bbb<as>(sadf[new a{\'b\' = "x"}]);}`;

        // act
        const res = parse(src);

        // assert
        expect.equal(res.content.length, 1);
        expectNode.injection(expect, res.content[0], 'let x = bbb<as>(sadf[new a{\'b\' = "x"}]);');
    });

    it("should drop all spaces if injection is only thing on the line", async expect => {
      // arrange
      // tslint:disable:no-trailing-whitespace
      const src = `abc
      @{let x = y;}     
def`;
      // act
      const res = parse(src);

      // assert
      expect.equal(res.content.length, 4);
      expectNode.literal(expect, res.content[0], "abc");
      expectNode.eol(expect, res.content[1]);      
      expectNode.injection(expect, res.content[2], "let x = y;");
      expectNode.literal(expect, res.content[3], "def");
  });
});
