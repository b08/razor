import * as expectNode from "./expectNode";
import { parse } from "../../src/parse/parse";
import { describe } from "@b08/test-runner";

describe("parser/literal", it => {
  it("should return literal node", async expect => {
    // arrange
    const src = "using asdf fre arfs";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 1);
    expectNode.literal(expect, res.content[0], src);
  });


  it("should return 2 literal nodes and eol in between", async expect => {
    // arrange
    const src = "aaa\nbbb";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 3);
    expectNode.literal(expect, res.content[0], "aaa");
    expectNode.eol(expect, res.content[1]);
    expectNode.literal(expect, res.content[2], "bbb");
  });


  it("should treat windows linebreaks the same way", async expect => {
    // arrange
    const src = "aaa\r\nbbb";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 3);
    expectNode.literal(expect, res.content[0], "aaa");
    expectNode.eol(expect, res.content[1]);
    expectNode.literal(expect, res.content[2], "bbb");
  });

  it("should treat mac linebreaks the same way", async expect => {
    // arrange
    const src = "aaa\rbbb";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 3);
    expectNode.literal(expect, res.content[0], "aaa");
    expectNode.eol(expect, res.content[1]);
    expectNode.literal(expect, res.content[2], "bbb");
  });


  it("should return 7 nodes", async expect => {
    // arrange
    const src = `aaa
        bbb
        ccc
        ddd`;

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 7);
  });


  it("should skip empty lines before literal", async expect => {
    // arrange
    const src = `

   aaa`;

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 1);
    expectNode.literal(expect, res.content[0], "   aaa");
  });


//   it("should not skip empty lines with force eol", async expect => {
//     // arrange
//     const src = `
//    @eol
//    aaa`;

//     // act
//     const res = parse(src);

//     // assert
//     expect.equal(res.content.length, 3);

//     expectNode.literal(res.content[0], "   ");
//     expectNode.forceEol(res.content[1]);
//     expectNode.literal(res.content[2], "   aaa");
//   });


//   it("should skip empty literal and eol after force eol", async expect => {
//     // arrange
//     const src = `
//    @eol
// `;

//     // act
//     const res = parse(src);

//     // assert
//     expect.equal(res.content.length, 2);
//     expectNode.literal(res.content[0], "   ");
//     expectNode.forceEol(res.content[1]);
//   });

//   it("should not skip non-empty literal after force eol", async expect => {
//     // arrange
//     const src = `
// @eol   aaa
// `;

//     // act
//     const res = parse(src);

//     // assert
//     expect.equal(res.content.length, 3);
//     expectNode.forceEol(res.content[0]);
//     expectNode.literal(res.content[1], "   aaa");
//     expectNode.eol(res.content[2]);
//   });

  it("should return quotes as quote node", async expect => {
    // arrange
    const src = `abc("def");`;

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 5);
    expectNode.literal(expect, res.content[0], "abc(");
    expectNode.quote(expect, res.content[1]);
    expectNode.literal(expect, res.content[2], "def");
    expectNode.quote(expect, res.content[3]);
    expectNode.literal(expect, res.content[4], ");");
  });

  it("should return apostrophes as quote node", async expect => {
    // arrange
    const src = `abc('def');`;

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 5);
    expectNode.literal(expect, res.content[0], "abc(");
    expectNode.apostrophe(expect, res.content[1]);
    expectNode.literal(expect, res.content[2], "def");
    expectNode.apostrophe(expect, res.content[3]);
    expectNode.literal(expect, res.content[4], ");");
  });

  it("should return escaped apostrophe and quote as literal", async expect => {
    // arrange
    const src = `abc(@'def@");`;

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 1);

    expectNode.literal(expect, res.content[0], `abc('def");`);
  });
});
