import * as expectNode from "./expectNode";
import { parse } from "../../src/parse/parse";
import { describe } from "@b08/test-runner";

describe("parser/if", it => {
  it("should return if node with literal node", async expect => {
    // arrange
    const src = "@if(let a of b){a}";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 1);
    const ifNode = expectNode.ifNode(expect, res.content[0], "let a of b");
    expectNode.ifChildren(expect, ifNode, 1);
    expectNode.literal(expect, ifNode.ifChildren[0], "a");
  });


  it("should return if node with else and literal nodes", async expect => {
    // arrange
    const src = "@if(let a of b){a}else{b}";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 1);
    const ifNode = expectNode.ifNode(expect, res.content[0], "let a of b");
    expectNode.ifChildren(expect, ifNode, 1, 1);
    expectNode.literal(expect, ifNode.ifChildren[0], "a");
    expectNode.literal(expect, ifNode.elseChildren[0], "b");
  });

  it("should return if node with spaced else and second curly brace", async expect => {
    // arrange
    const src = "@if(let a of b){a}   else   {b}";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 1);
    const ifNode = expectNode.ifNode(expect, res.content[0], "let a of b");
    expectNode.ifChildren(expect, ifNode, 1, 1);
    expectNode.literal(expect, ifNode.ifChildren[0], "a");
    expectNode.literal(expect, ifNode.elseChildren[0], "b");
  });

  it("should treat else as content if curly brace not found", async expect => {
    // arrange
    const src = "@if(let a of b){a}else a";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 2);
    const ifNode = expectNode.ifNode(expect, res.content[0], "let a of b");
    expectNode.ifChildren(expect, ifNode, 1, 0);
    expectNode.literal(expect, ifNode.ifChildren[0], "a");
    expectNode.literal(expect, res.content[1], "else a");
  });

  it("should return multiline if and else with partials", async expect => {
    // arrange
    const src = `
  @if(imp.isMultiline) {
    @[multiLine(imp)]
  } else {
    @[singleLine(imp)]
  }
`;

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 1);
    const ifNode = expectNode.ifNode(expect, res.content[0], "imp.isMultiline");
    expectNode.ifChildren(expect, ifNode, 1, 1);
    expectNode.partial(expect, ifNode.ifChildren[0], "multiLine", "imp", "    ");
    expectNode.partial(expect, ifNode.elseChildren[0], "singleLine", "imp", "    ");
  });


  it("should return if multiline node with else", async expect => {
    // arrange
    const src = `@if(let a of b){
            a
        } else {b}`;

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 1);
    const ifNode = expectNode.ifNode(expect, res.content[0], "let a of b");
    expectNode.ifChildren(expect, ifNode, 2, 1);
    expectNode.literal(expect, ifNode.ifChildren[0], "            a");
    expectNode.eol(expect, ifNode.ifChildren[1]);
    expectNode.literal(expect, ifNode.elseChildren[0], "b");
  });


  it("should return if node with multiline else", async expect => {
    // arrange
    const src = `@if(let a of b){a} else {
            b
        }`;

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 1);
    const ifNode = expectNode.ifNode(expect, res.content[0], "let a of b");
    expectNode.ifChildren(expect, ifNode, 1, 2);
    expectNode.literal(expect, ifNode.ifChildren[0], "a");
    expectNode.literal(expect, ifNode.elseChildren[0], "            b");
    expectNode.eol(expect, ifNode.elseChildren[1]);
  });
});
