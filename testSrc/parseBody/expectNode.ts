import { IExpect } from "@b08/test-runner";
import { BasicNode, NodeType, ContentNode, ForNode, IfNode, PartialNode } from "../../src/parse/nodes/nodes.type";

function expectNodeType(expect: IExpect, node: BasicNode, type: NodeType): void {
  expect.equal(node.type, type);
}

function contentNode(expect: IExpect, node: BasicNode, content: string, type: NodeType): void {
  expectNodeType(expect, node, type);
  const contentNode = node as ContentNode;
  expect.equal(contentNode.content, content);
}

export function comment(expect: IExpect, node: BasicNode, content: string): void {
  contentNode(expect, node, content, NodeType.Comment);
}

export function literal(expect: IExpect, node: BasicNode, content: string): void {
  contentNode(expect, node, content, NodeType.Literal);
}

export function quote(expect: IExpect, node: BasicNode): void {
  expectNodeType(expect, node, NodeType.Quote);
}

export function apostrophe(expect: IExpect, node: BasicNode): void {
  expectNodeType(expect, node, NodeType.Apostrophe);
}

export function expression(expect: IExpect, node: BasicNode, content: string): void {
  contentNode(expect, node, content, NodeType.Expression);
}

export function injection(expect: IExpect, node: BasicNode, content: string): void {
  contentNode(expect, node, content, NodeType.Injection);
}

export function eol(expect: IExpect, node: BasicNode): void {
  expectNodeType(expect, node, NodeType.Eol);
}

export function forceEol(expect: IExpect, node: BasicNode): void {
  expectNodeType(expect, node, NodeType.ForceEol);
}


export function forEach(expect: IExpect, node: BasicNode, condition: string, childCount?: number): ForNode {
  expectNodeType(expect, node, NodeType.For);
  const forEachNode = node as ForNode;
  expect.equal(forEachNode.condition, condition);
  if (childCount) {
    expect.equal(forEachNode.children.length, childCount, "child count in forEach node is not as expected");
  }
  return forEachNode;
}

export function ifNode(expect: IExpect, node: BasicNode, condition: string): IfNode {
  expectNodeType(expect, node, NodeType.If);
  const ifNode = node as IfNode;
  expect.equal(ifNode.condition, condition);
  return ifNode;
}

export function ifChildren(expect: IExpect, node: IfNode, ifCount: number, elseCount: number = 0): void {
  expect.equal(node.ifChildren.length, ifCount, "if node children count is not as expected");
  expect.equal(node.elseChildren.length, elseCount, "if node children else count is not as expected");
}

export function partial(expect: IExpect, node: BasicNode, name: string, parameters: string, indent: string): void {
  expectNodeType(expect, node, NodeType.Partial);
  const partialNode = node as PartialNode;
  expect.equal(partialNode.name, name);
  expect.equal(partialNode.parameters, parameters);
  expect.equal(partialNode.indent, indent);
}
