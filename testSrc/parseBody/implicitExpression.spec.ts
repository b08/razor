import * as expectNode from "./expectNode";
import { parse } from "../../src/parse/parse";
import { describe } from "@b08/test-runner";

describe("parser/implicitExpression", it => {

  it("should treat @@ as escaped @", async expect => {
    // arrange
    const src = "aaa@@bbb ccc";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 1);
    expectNode.literal(expect, res.content[0], "aaa@bbb ccc");
  });


  it("should return expression node", async expect => {
    // arrange
    const src = "aaa@bbb ccc";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 3);
    expectNode.literal(expect, res.content[0], "aaa");
    expectNode.expression(expect, res.content[1], "bbb");
    expectNode.literal(expect, res.content[2], " ccc");
  });


  it("should return 2 expression nodes in a row", async expect => {
    // arrange
    const src = "@bbb@ccc";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 2);
    expectNode.expression(expect, res.content[0], "bbb");
    expectNode.expression(expect, res.content[1], "ccc");
  });


  it("should return expression node and eol node", async expect => {
    // arrange
    const src = "@bbb\nccc";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 3);
    expectNode.expression(expect, res.content[0], "bbb");
    expectNode.eol(expect, res.content[1]);
    expectNode.literal(expect, res.content[2], "ccc");
  });

  it("should return implicit expression with content in brackets", async expect => {
    // arrange
    const src = "aaa @bbb[vvv].ccc asd";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 3);
    expectNode.literal(expect, res.content[0], "aaa ");
    expectNode.expression(expect, res.content[1], "bbb[vvv].ccc");
    expectNode.literal(expect, res.content[2], " asd");
  });


  it("should return (at) as part of literal if expressions has no meaningful characters", async expect => {
    // arrange
    const src = "@)";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 1);
    expectNode.literal(expect, res.content[0], src);
  });
});
