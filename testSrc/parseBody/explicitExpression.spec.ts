import * as expectNode from "./expectNode";
import { parse } from "../../src/parse/parse";
import { describe } from "@b08/test-runner";

describe("parser/explicitExpression", it => {

    it("should return expression node", async expect => {
        // arrange
        const src = "aaa@(bbb) ccc";

        // act
        const res = parse(src);

        // assert
        expect.equal(res.content.length, 3);
        expectNode.literal(expect, res.content[0], "aaa");
        expectNode.expression(expect, res.content[1], "bbb");
        expectNode.literal(expect, res.content[2], " ccc");
    });


    it("should return 2 expression nodes in a row", async expect => {
        // arrange
        const src = "@(bbb)@(ccc)";

        // act
        const res = parse(src);

        // assert
        expect.equal(res.content.length, 2);
        expectNode.expression(expect, res.content[0], "bbb");
        expectNode.expression(expect, res.content[1], "ccc");
    });


    it("should return deep brackets in explicit expression", async expect => {
        // arrange
        const src = '@(bbb<as>(sadf[new a{\'b\' = "x"}]))';

        // act
        const res = parse(src);

        // assert
        expect.equal(res.content.length, 1);
        expectNode.expression(expect, res.content[0], 'bbb<as>(sadf[new a{\'b\' = "x"}])');
    });
});
