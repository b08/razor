import * as expectNode from "./expectNode";
import { parse } from "../../src/parse/parse";
import { describe } from "@b08/test-runner";

describe("parser/for", it => {
  it("should return foreach node with literal node", async expect => {
    // arrange
    const src = "@foreach(let a of b){a}";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 1);
    const forEach = expectNode.forEach(expect, res.content[0], "let a of b", 1);
    expectNode.literal(expect, forEach.children[0], "a");
  });


  it("should return foreach node with spaced curly brace", async expect => {
    // arrange
    const src = "@foreach(let a of b)   {a}";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 1);
    const forEach = expectNode.forEach(expect, res.content[0], "let a of b", 1);
    expectNode.literal(expect, forEach.children[0], "a");
  });


  it("should fallback from foreach if no brace found", async expect => {
    // arrange
    const src = "@foreach(let a of b)   abc";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 1);
    expectNode.literal(expect, res.content[0], "@foreach(let a of b)   abc");
  });


  it("should fallback from foreach if eof", async expect => {
    // arrange
    const src = "a@foreach(let a of b)";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 1);
    expectNode.literal(expect, res.content[0], "a@foreach(let a of b)");
  });

  it("should return foreach node with deep brackets inside condition", async expect => {
    // arrange
    const src = '@foreach(let a of b(x["z" < \'d\'])){a}';

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 1);
    const forEach = expectNode.forEach(expect, res.content[0], 'let a of b(x["z" < \'d\'])', 1);
    expectNode.literal(expect, forEach.children[0], "a");
  });


  it("should treat closing curly brace as text if prepended by literal on the same line", async expect => {
    // arrange
    const src =
      `@foreach(let a of b){
    aasd{f}
}`;

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 1);
    const forEach = expectNode.forEach(expect, res.content[0], "let a of b", 2);
    expectNode.literal(expect, forEach.children[0], "    aasd{f}");
    expectNode.eol(expect, forEach.children[1]);
  });


  it("should treat escaped curly brace as text", async expect => {
    // arrange
    const src =
      `@foreach(let a of b){
    aasd{f}
    @}
}`;

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 1);
    const forEach = expectNode.forEach(expect, res.content[0], "let a of b", 4);
    expectNode.literal(expect, forEach.children[0], "    aasd{f}");
    expectNode.eol(expect, forEach.children[1]);
    expectNode.literal(expect, forEach.children[2], "    }");
    expectNode.eol(expect, forEach.children[3]);
  });


  it("should return foreach node with 2 lines of literals", async expect => {
    // arrange
    const src =
      `@foreach(let a of b){
    aasdf
    sdfsg
}`;

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 1);
    const forEach = expectNode.forEach(expect, res.content[0], "let a of b", 4);
    expectNode.literal(expect, forEach.children[0], "    aasdf");
    expectNode.eol(expect, forEach.children[1]);
    expectNode.literal(expect, forEach.children[2], "    sdfsg");
    expectNode.eol(expect, forEach.children[3]);
  });


  it("should return foreach inside foreach", async expect => {
    // arrange
    const src = `@foreach(let a of b){
    aasdf
    @foreach(let c of a){
    sdfsg
    }
}`;

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 1);
    const forEach = expectNode.forEach(expect, res.content[0], "let a of b", 3);
    expectNode.literal(expect, forEach.children[0], "    aasdf");
    expectNode.eol(expect, forEach.children[1]);

    const forEach2 = expectNode.forEach(expect, forEach.children[2], "let c of a", 2);
    expectNode.literal(expect, forEach2.children[0], "    sdfsg");
    expectNode.eol(expect, forEach2.children[1]);
  });
});
