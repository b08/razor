import * as expectNode from "./expectNode";
import { parse } from "../../src/parse/parse";
import { describe } from "@b08/test-runner";

describe("parser/brackets", it => {
  it("should return parentheses as part of expression node", async expect => {
    // arrange
    const src = "@a(b).c d";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 2);
    expectNode.expression(expect, res.content[0], "a(b).c");
    expectNode.literal(expect, res.content[1], " d");
  });


  it("should return square brackets as part of expression node", async expect => {
    // arrange
    const src = "@a[b].c d";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 2);
    expectNode.expression(expect, res.content[0], "a[b].c");
    expectNode.literal(expect, res.content[1], " d");
  });


  it("should return angle brackets as part of expression node", async expect => {
    // arrange
    const src = "@a<b>(c) d";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 2);
    expectNode.expression(expect, res.content[0], "a<b>(c)");
    expectNode.literal(expect, res.content[1], " d");
  });


  it("should treat other brackets inside angle bracket as text", async expect => {
    // arrange
    const src = "@a<[({> d";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 2);
    expectNode.expression(expect, res.content[0], "a<[({>");
    expectNode.literal(expect, res.content[1], " d");
  });

  it("should return deep brackets as part of expression node", async expect => {
    // arrange
    const src = "@a(c[d<b>()(new a{b=1})]) d";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 2);
    expectNode.expression(expect, res.content[0], "a(c[d<b>()(new a{b=1})])");
    expectNode.literal(expect, res.content[1], " d");
  });


  it("should treat angle bracket as text if followed by space", async expect => {
    // arrange
    const src = "@a(b < c) d";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 2);
    expectNode.expression(expect, res.content[0], "a(b < c)");
    expectNode.literal(expect, res.content[1], " d");
  });


  it("should return quotes as part of expression node", async expect => {
    // arrange
    const src = '@a("b") d';

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 2);
    expectNode.expression(expect, res.content[0], 'a("b")');
    expectNode.literal(expect, res.content[1], " d");
  });


  it("should respect escapes inside quotes", async expect => {
    // arrange
    const src = '@a(a["b\\"a"]) d';

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 2);
    expectNode.expression(expect, res.content[0], 'a(a["b\\"a"])');
    expectNode.literal(expect, res.content[1], " d");
  });


  it("should return apostrophes as part of expression node", async expect => {
    // arrange
    const src = "@a('b') d";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 2);
    expectNode.expression(expect, res.content[0], "a('b')");
    expectNode.literal(expect, res.content[1], " d");
  });


  it("should respect escapes inside apostrophes", async expect => {
    // arrange
    const src = "@a(a['b\\'a']) d";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 2);
    expectNode.expression(expect, res.content[0], "a(a['b\\'a'])");
    expectNode.literal(expect, res.content[1], " d");
  });


  it("should return curly braces as part of expression node", async expect => {
    // arrange
    const src = "@a(new b{x = 1}) d";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 2);
    expectNode.expression(expect, res.content[0], "a(new b{x = 1})");
    expectNode.literal(expect, res.content[1], " d");
  });

});
