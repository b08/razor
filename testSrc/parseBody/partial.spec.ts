import * as expectNode from "./expectNode";
import { parse } from "../../src/parse/parse";
import { describe } from "@b08/test-runner";

describe("parser/partial", it => {

  it("should return partial node with no indent", async expect => {
    // arrange
    const src = "@[aaa(bbb)]";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 1);
    expectNode.partial(expect, res.content[0], "aaa", "bbb", "");
  });

  it("should return partial node with indent", async expect => {
    // arrange
    const src = "   @[aaa(bbb)]";

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 1);
    expectNode.partial(expect, res.content[0], "aaa", "bbb", "   ");
  });


  it("should return partial node with indent after eol", async expect => {
    // arrange
    const src = `hi
    @[aaa(bbb)]`;

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 3);
    expectNode.literal(expect, res.content[0], "hi");
    expectNode.eol(expect, res.content[1]);
    expectNode.partial(expect, res.content[2], "aaa", "bbb", "    ");
  });

  it("should ignore eol if return partial node with indent after eol", async expect => {
    // arrange
    const src = `hi
    @[aaa(bbb)]
    abc`;

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 4);
    expectNode.literal(expect, res.content[0], "hi");
    expectNode.eol(expect, res.content[1]);
    expectNode.partial(expect, res.content[2], "aaa", "bbb", "    ");
    expectNode.literal(expect, res.content[3], "    abc");
  });

  it("should ignore eol if return partial node without indent", async expect => {
    // arrange
    const src = `
@[aaa(bbb)]
    abc`;

    // act
    const res = parse(src);

    // assert
    expect.equal(res.content.length, 2);
    expectNode.partial(expect, res.content[0], "aaa", "bbb", "");
    expectNode.literal(expect, res.content[1], "    abc");
  });
});
