import { test } from "@b08/test-runner";

test("razor should produce generator that works with partial", async expect => {
  // arrange
  const srcFile = "../../testData/partialClass/resultClass.rzr";
  const { resultClass } = await import(srcFile);
  const expected =
    `export class ResultClass {
  public field4: string = "field4";
}`;

  // act
  const result = resultClass.generate();

  // assert
  expect.true(result.indexOf(expected) !== -1);
});
