import { test } from "@b08/test-runner";

test("razor generator should produce a class with 2 fields", async expect => {
  // arrange
  const srcFile = "../../testData/classWithIteratedFields/resultClass.rzr";
  const { resultClass } = await import(srcFile);
  const info = { fieldNames: ["field2", "field3"] };
  const expected =
`export class ResultClass {

  public field2: string = "field2";

  public field3: string = "field3";
}`;

  // act
  const result = resultClass.generate(info);

  // assert
  expect.true(result.indexOf(expected) !== -1);
});
