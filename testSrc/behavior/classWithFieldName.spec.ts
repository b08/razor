import { test } from "@b08/test-runner";

test("razor generator should produce a class with field", async expect => {
  // arrange
  const srcFile = "../../testData/classWithFieldName/resultClass.rzr";
  const { resultClass } = await import(srcFile);
  const info = { fieldName: "field1" };
  const expected =
    `export class ResultClass {
  public field1: string = "abc";
}`;

  // act
  const result = resultClass.generate(info);

  // assert
  expect.true(result.indexOf(expected) !== -1);
});
