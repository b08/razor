import { test } from "@b08/test-runner";

test("razor generator should produce a class with only a second field", async expect => {
  // arrange
  const srcFile = "../../testData/classWithCondition/resultClass.rzr";
  const { resultClass } = await import(srcFile);
  const info = { addField1: false, addField2: true };
  const expected =
    `export class ResultClass {
  public field2: string = "field2";
}`;

  // act
  const result = resultClass.generate(info);

  // assert
  expect.true(result.indexOf(expected) !== -1);
});
