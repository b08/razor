import { describe } from "@b08/test-runner";
import { parse } from "../../src/parse/parse";
import { ParsedConfig, parsedImport } from "../../src/parse/types/parsedConfig.type";

export const defaultDefinition = {
  position: -1,
  name: "generator",
  parameters: ""
};

describe("parseConfig", it => {
  it("should make a default", async expect => {
    // arrange
    const source = "";

    const expected: ParsedConfig = {
      imports: [],
      definition: defaultDefinition
    };

    // act
    const result = parse(source).config;

    // assert
    expect.deepEqual(result, expected);
  });

  it("should parse imports", async expect => {
    // arrange
    const source = `@import { a } from 'b'
@import { b } from 'c'
`;

    const expected: ParsedConfig = {
      imports: [
        parsedImport(0, "{ a } from 'b'"),
        parsedImport(23, "{ b } from 'c'"),
      ],
      definition: defaultDefinition
    };

    // act
    const result = parse(source).config;

    // assert
    expect.deepEqual(result, expected);
  });

  it("should parse definition", async expect => {
    // arrange
    const source = "@define imports(a: number)\n";

    const expected: ParsedConfig = {
      imports: [],
      definition: {
        position: 0,
        parameters: "a: number",
        name: "imports"
      }
    };

    // act
    const result = parse(source).config;

    // assert
    expect.deepEqual(result, expected);
  });

  it("should read past space tokens", async expect => {
    // arrange
    const source = "\n  \n \n\n@import { a } from 'b'\n";

    const expected: ParsedConfig = {
      imports: [parsedImport(7, "{ a } from 'b'"), ],
      definition: defaultDefinition
    };

    // act
    const result = parse(source).config;

    // assert
    expect.deepEqual(result, expected);
  });

  it("should stop at unknown non-space token", async expect => {
    // arrange
    const source = "  \n \n\n unknown";

    const expected: ParsedConfig = {
      imports: [],
      definition: defaultDefinition
    };

    // act
    const result = parse(source).config;

    // assert
    expect.deepEqual(result, expected);
  });
});
